=== Enrich RSS Category Elements ===
Contributors: simongibbs596
Donate link: http://libertarianhome.co.uk/enrich-categories/
Tags: RSS
Requires at least: 3.3.1
Tested up to: 3.3.1
Stable tag: 1.0

Adds the category base URL as a "domain" attribute on RSS category elements that correspond to the Wordpress category
assigned to the post. Category elements that correspond to tags are not changed, allowing you to tell the difference.

Also adds the non-standard "href" attribute, enabling consumers to display links to related content.

== Description ==

Adds the category base URL as a "domain" attribute on RSS category elements that correspond to the Wordpress category
assigned to the post. Category elements that correspond to tags are not changed, allowing you to tell the difference.

Also adds the non-standard "href" attribute, enabling consumers to display links to related content.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `discat.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Isn't this handy! =

I thought so.


== Changelog ==

= 1.0 =
* Initial version
