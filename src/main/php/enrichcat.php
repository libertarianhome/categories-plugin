<?php
/*
Plugin Name: Enrich RSS Category Elements
Plugin URI: http://libertarianhome.co.uk/enrich-categories/
Description: Adds domain and href attributes to category elements in your RSS to help advanced consumers access your site structure.
Version: 1.0
Author: Simon Gibbs
Author URI: http://libertarianhome.co.uk/author/admin/
Author Email: simon.gibbs@libertarianhome.co.uk
License: Copyright &copy; Cantorva Limited

*/


class LHCategoryDisambiguator {


/*--------------------------------------------*
* Constructor
*--------------------------------------------*/

    /**
     * Initializes the plugin by setting localization, filters, and administration functions.
     */
    function __construct() {

        /*
        * TODO:
        * Define the custom functionality for your plugin. The first parameter of the
        * add_action/add_filter calls are the hooks into which your code should fire.
        *
        * The second parameter is the function name located within this class. See the stubs
        * later in the file.
        *
        * For more information:
        * http://codex.wordpress.org/Plugin_API#Hooks.2C_Actions_and_Filters
        */

        add_filter('the_category_rss', array( $this, 'enrich_rss_categories'), 10, 2);

    } // end constructor


    function enrich_rss_categories( $the_list, $type ) {

        if ( empty($type) )
            $type = get_default_feed();

        if ( 'rdf' == $type )
            return $the_list;

        if ( 'atom' == $type )
            return $the_list;


        $categories = get_the_category();

        $site_url = site_url();
        $blog_charset = get_option('blog_charset');

        if ( !empty($categories) ) foreach ( (array) $categories as $category ) {
            $cat_name = sanitize_term_field('name', $category->name, $category->term_id, 'category', 'rss');
            $cat_slug =  $category->slug;

            $cat_name_markup ="<category><![CDATA[" . @html_entity_decode( $cat_name, ENT_COMPAT, $blog_charset ) . "]]></category>";
            $modified_markup = str_replace("<category>", "<category domain=\"$site_url/category/\" href=\"$site_url/category/$cat_slug\" >", $cat_name_markup);
            $the_list = str_replace($cat_name_markup, $modified_markup, $the_list);

        }


        return $the_list;
    }



} // end class

$enrichcat = new LHCategoryDisambiguator();
